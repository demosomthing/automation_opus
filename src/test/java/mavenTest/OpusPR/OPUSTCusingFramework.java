package mavenTest.OpusPR;



import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class OPUSTCusingFramework {

	WebDriver driver = null;

	
	@Test
	public void OpusFlow() throws InterruptedException
	{
		OpusOperations launchOpus = new OpusOperations();
		//OpusOperations globalLogon = new OpusOperations();
		OPUSCreateCustOps CreateCust = new OPUSCreateCustOps();
		
		ServiceProfileForNewCust service = new ServiceProfileForNewCust();
		BuildMyBundle bb_page = new BuildMyBundle();
		BuildBundle2 bb_page2 = new BuildBundle2();
		CreateAccount cdetails = new CreateAccount();
		CreateAccount2 custSecurityDetails= new CreateAccount2();
		createAccountCreditResult creditRes = new createAccountCreditResult();
		InstallAndInstallment 	Ndd = new InstallAndInstallment();		
		ReviewAndSubmit install = new ReviewAndSubmit();
		ReviewOrder review = new ReviewOrder();
		
		ReviewOrderCont reviewCont = new ReviewOrderCont();
		
	
		
		UnifiedCreditPolicyReq ucpReq = new UnifiedCreditPolicyReq();
		
		SubmitOrder sborder = new SubmitOrder();
		TaxInformation taxinfo = new TaxInformation();
		
		Customer_Checkout Check = new Customer_Checkout();
		
		// CustCheckout_sign sign = new CustCheckout_sign();
		
		// ReceiptPreference rpref = new ReceiptPreference();
		
		OrderConfirmation confirmorder = new OrderConfirmation();
		//-------------------------------------
		driver = launchOpus.launchOpus(driver);
		//-------------------------------------
		
		Thread.sleep(5000);
		
		//------------------------------------
		driver = launchOpus.globalLogon(driver);
		//------------------------------------
		
		Thread.sleep(10000);
		
		driver = CreateCust.enterAddress(driver);
		
		//------------------------------------
		
		Thread.sleep(10000);
				
		driver = service.serviceAvailability(driver);
		
		//------------------------------------
		
		Thread.sleep(5000);
		driver= bb_page.selectOffers(driver);
	
		//------------------------------------
	
		Thread.sleep(10000);
		driver= bb_page2.bundleSummary(driver);
		
		//-----------------------------------------
		Thread.sleep(8000);
		driver= cdetails.accountDetails(driver) ;
		
		//-----------------------------------------
		
		Thread.sleep(8000);				
		driver= custSecurityDetails.createAccount2Page(driver);
		
		//-----------------------------------------
		Thread.sleep(1000);	
		driver = creditRes.creditResult(driver);
		
		//-----------------------------------------
		Thread.sleep(1000);					
		driver= Ndd.installation(driver);
		
		//-----------------------------------------
		Thread.sleep(5000);	
		driver = install.reviewBilling(driver);	
		
		
		//-----------------------------------------
				Thread.sleep(2000);	
				driver = review.orderReview(driver);	
			
				//-----------------------------------------
				Thread.sleep(2000);	
				driver = reviewCont.orderReviewCont(driver);	
				
				//-----------------------------------------
				
				//-----------------------------------------
				Thread.sleep(5000);	
				driver = ucpReq.ucpReqPage(driver);	
				
				//-----------------------------------------
				Thread.sleep(2000);	
				driver = sborder.submitOrderPage(driver);	
				
				//-----------------------------------------
				Thread.sleep(2000);	
				driver = taxinfo.taxAndTypeInformat(driver);
			
				//-----------------------------------------
				Thread.sleep(4000);	
			driver = Check.checkout(driver);
			
			//-----------------------------------------
			Thread.sleep(4000);	
		driver = confirmorder.Orderconfirm(driver);
		
		//-----------------------------------------
//		Thread.sleep(4000);	
//		driver = sign.custSign(driver);

		//-----------------------------------------
//		Thread.sleep(5000);
//		driver = rpref.receiptPref(driver);
		
		
		
	}

}
