package mavenTest.OpusPR;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import lib.ExcelDataConfig;
import lib.Locators;

public class OpusOperations {
	
	ExcelDataConfig excel = new ExcelDataConfig();

	public WebDriver launchOpus(WebDriver driver) 
	{
		System.setProperty("webdriver.ie.driver","C:\\ETMtoolautomation\\OpusAutomation\\Exes\\IEDriverServer.exe");
	

		
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		
		InternetExplorerOptions IEoptions=new InternetExplorerOptions();
		IEoptions.ignoreZoomSettings();
		IEoptions.introduceFlakinessByIgnoringSecurityDomains();
		IEoptions.destructivelyEnsureCleanSession();
		IEoptions.useCreateProcessApiToLaunchIe();
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		driver= new InternetExplorerDriver(IEoptions);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		IEoptions.withInitialBrowserUrl("about:blank");
		
		String url="";
		url = excel.getData(0, 1, 1);
		driver.get(url);
		
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		//driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		
	
		try {
			Thread.sleep(2000);
			String storeSelect;
			storeSelect= excel.getData(0, 1, 2);
			
			WebElement storeid = driver.findElement(Locators.storeid);
			Thread.sleep(3000);
			Select store = new Select(storeid);
			store.selectByVisibleText(storeSelect); 
			driver.findElement(Locators.clickLogin).click();
			System.out.println("store selected");
			
		}
		catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
		
		return driver;
		
	}

	public WebDriver globalLogon(WebDriver driver) throws InterruptedException
	{	
		//String storeSelect;
		//storeSelect= excel.getData(0, 1, 2);
		
		//WebElement storeid = driver.findElement(Locators.storeid);
		//Thread.sleep(3000);
		//Select store = new Select(storeid);
		//store.selectByVisibleText(storeSelect); 
		//driver.findElement(Locators.clickLogin).click();
		//System.out.println("done");
		String userid =""; 
		userid = excel.getData(0, 1, 3);
		String password ="";
		password=excel.getData(0, 1, 4);
		Thread.sleep(10000);
		System.out.println("userid is "+userid+ ","+"password is: "+password);
		driver.findElement(Locators.Userid).sendKeys(userid);
		driver.findElement(Locators.pwd).sendKeys(password);
		
		
		WebElement btnSubmit =driver.findElement(Locators.clickSubmit);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", btnSubmit);
		
		Thread.sleep(1000);
		
		//----------------------------- LOG ON Succesful Page -------------------------------//
		
		WebElement successOK = driver.findElement(Locators.clickOK);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("arguments[0].click();", successOK);
			
		System.out.println("---------------Waiting to Load OPUS Home Page -------------------------- ");
		Thread.sleep(5000);
		
		return driver;
		
	}
	
		
}
