package mavenTest.OpusPR;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



import lib.ExcelDataConfig;
import lib.Locators;
import lib.Screenshot;

public class OPUSCreateCustOps 
{
	// ExcelDataConfig excel = new ExcelDataConfig("C:\\Selenium\\OPUS\\Test Data\\Test_Details.xlsx");
	ExcelDataConfig excel = new ExcelDataConfig();
	String address_line1,  zip_Code;

	public WebDriver enterAddress(WebDriver driver) throws InterruptedException
	{
		try {
			String pageTitle = driver.getTitle();
			System.out.println("Title of the current page is : " + pageTitle);
			driver.switchTo().frame("VISIFRAME");
			WebElement processLater = driver.findElement(Locators.processLater);

			if (processLater.isDisplayed()) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", processLater);
				Thread.sleep(2000);
				newCustomerAddr( driver,address_line1, zip_Code);
			}

			return driver;					
		}
		catch (Exception e) {
			Thread.sleep(2000);				
			System.out.println("Process Later page not dispalyed");
			
			
			newCustomerAddr( driver,address_line1, zip_Code);
		
			return driver;
		}

	}

	
	public void newCustomerAddr(WebDriver driver, String address_line1,String zip_Code) throws InterruptedException
	{
		
		driver.switchTo().defaultContent();
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		driver.switchTo().frame("VISIFRAME");
		System.out.println(driver.getPageSource().toString().substring(0, 100));
		Thread.sleep(3000);
		WebElement newCust = driver.findElement(Locators.newCust);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", newCust);


		address_line1 = excel.getData(1, 1, 0);

		//city = excel.getData(1, 1, 1);

		zip_Code = excel.getData(1, 1, 3);
		//String state = null;


		driver.findElement(Locators.address1).sendKeys(address_line1);
		//driver.findElement(Locators.addressCity).sendKeys(city);
		driver.findElement(Locators.zip).sendKeys(zip_Code);
		Thread.sleep(4000);
		
		Screenshot.captureScreenshot(driver,"NewCustAddress---1");
		
		Thread.sleep(5000); 
		
		//*------------Click on Check Availability Button --------------------*//
		
		WebElement checkAvail = driver.findElement(Locators.checkAvailabilty);
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("arguments[0].click();", checkAvail );

		System.out.println("---------------------Waiting to Load Service Profile Page -----------------------");
		
		}
		
	
	}





