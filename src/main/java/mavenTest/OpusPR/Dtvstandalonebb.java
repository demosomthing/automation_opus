package mavenTest.OpusPR;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import lib.ExcelDataConfig;
import lib.Locators;
import lib.Screenshot;

public class Dtvstandalonebb {
	
	ExcelDataConfig excel = new ExcelDataConfig();
	
	
	public WebDriver selectOffersdtv(WebDriver driver)
		{
			try 
				{
				
				Thread.sleep(80000);	
		
				Screenshot.captureScreenshot(driver, "DTV Card before selection");
				
		
				Thread.sleep(2000);
				
				WebElement clickdtvoffer = driver.findElement(By.xpath("//*[@selectedofferid='DTV_ChoiceAll_DV0013']"));
				
				Screenshot.captureScreenshot(driver, "DTV Card before selection2");
				
				Thread.sleep(2000);
				
				clickdtvoffer.click();
		
		
				System.out.println("dtv selected");
		
		Thread.sleep(50000);
		
		Screenshot.captureScreenshot(driver, "dtv popup----6");
		
		WebElement clickPopup = driver.findElement(By.xpath("//*[@selectedofferid='DTV_BASE_PROMO_DS1213']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", clickPopup);
		
		Thread.sleep(1000);	
		
		WebElement clickPopup3 = driver.findElement(By.xpath("//*[@selectedofferid='DTV_PICK_HBO_ADD_PROMO_DS1199']"));
		
		js.executeScript("arguments[0].click();", clickPopup3);
		
		//Thread.sleep(1000);	
		
		WebElement clickPopup4 = driver.findElement(By.xpath("//*[@selectedofferid='DTV_PICK_BASE_PROMO_DS1201']"));
		
		js.executeScript("arguments[0].click();", clickPopup4);
		
		Thread.sleep(1000);	
		
		Screenshot.captureScreenshot(driver, "dtv Accepted popup offers");
		
		Thread.sleep(5000);	
		
		
		WebElement clickdtvpopupoffers = driver.findElement(By.xpath("//*[@id='rtpDisclosureOverlayContinueButton']"));
		
		clickdtvpopupoffers.click();
		
		System.out.println(">>>>>>>>popup closed<<<<<<<<<");
		
		Thread.sleep(4000);
		
		
		WebElement clicktvs = driver.findElement(By.xpath("//*[@receiverid='TVCount_1']"));
		
		clicktvs.click();
		
		Thread.sleep(4000);
		WebElement clickreceivers= driver.findElement(By.xpath("//*[@receiverid='DTVHDonly_1']"));
		
		clickreceivers.click();
		
		Screenshot.captureScreenshot(driver, "dtv receiver----6");
		Thread.sleep(4000);
	
		WebElement valiadteorder= driver.findElement(Locators.validateQuote);
		
		js.executeScript("arguments[0].click();", valiadteorder);
				
	
			
	}
	
	catch (Exception e) 
	{
		
		e.printStackTrace();
	}
		return driver;
		
		}
}
	
	
	
	