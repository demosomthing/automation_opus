package mavenTest.OpusPR;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import lib.ExcelDataConfig;
import lib.Locators;
import lib.Screenshot;


public class ServiceProfileForDTVandVoipDoubleplay {


	ExcelDataConfig excel = new ExcelDataConfig();
	String dtv,hsia,iptv,voip,wls;
	
	public WebDriver serviceAvailability(WebDriver driver) {
		
		System.out.println(" Waiting to Load ------Service Profile For New Customer page -------------- ");
		
		for(int i=0 ;i<40;i++)
		{						
			try {
				String pagename = driver.findElement(By.xpath("//*[@class ='headerBarblock']")).getText();
				System.out.println(pagename);
				
				Screenshot.captureScreenshot(driver,"AvailableServices---2");
				
				Thread.sleep(4000);
				WebElement ClickLater = driver.findElement(Locators.ClickLater);
				WebElement beginorder = driver.findElement(Locators.Beginorder);
				
				if(beginorder.isDisplayed())
				{
					i=40;
									
					String pageTitle = driver.getTitle();
					System.out.println("Title of the current page is : " + pageTitle);		
					services_Excel(driver,hsia );
					Thread.sleep(3000);
		
						// WebElement beginorder = driver.findElement(Locators.Beginorder);
					
						JavascriptExecutor js = (JavascriptExecutor) driver;
															
						js.executeScript("arguments[0].click();", ClickLater );	
						
						Thread.sleep(3000);
						
						js.executeScript("arguments[0].click();", beginorder );
					
						System.out.println("---------------Waiting to Load Build My Bundle Page --------------------");
				}
				
				else
				{
					Thread.sleep(10000);
				}
			}
			catch(Exception e)
			{
				System.out.println("Exception is : "+ e.getMessage());
			}
		}
				
		return driver;
		
	}
	
	

	public void services_Excel(WebDriver driver ,String hsia ) throws Exception
	{
		//Read the services to be added from Excel
		
	
	
	System.out.println("===================HSIA Selection====================");
	 hsiaService(driver);
	
		
	}
	
	
	
	public void hsiaService(WebDriver driver)
	{
		hsia =excel.getData(1, 1, 6);
	if(hsia.equals("Y"))
		{
		boolean status =driver.findElement(Locators.hsia).isEnabled();
		if(status==true)
		{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		/*js.executeScript("arguments[0].click();", driver.findElement(Locators.hsia));
		Screenshot.captureScreenshot(driver, "HSIA selection");
		System.out.println(" HSIA Selected ");*/
		
		js.executeScript("arguments[0].click();", driver.findElement(Locators.dtv));
		Screenshot.captureScreenshot(driver, "DTV selection");
		System.out.println(" dtv Selected "); 
		
		
		js.executeScript("arguments[0].click();", driver.findElement(Locators.voip));
		Screenshot.captureScreenshot(driver, "voip selection");
		System.out.println(" voip Selected ");
		
		
		js.executeScript("arguments[0].click();", driver.findElement(Locators.ClickLater));
		Screenshot.captureScreenshot(driver, "creditchecklater");
		System.out.println(" Credit check cancelled");
		
		}
		else 
		{
			System.out.println(" one of the Service checkbox is disabled");
		}
	}
	else
	 {
		 System.out.println(" services is not selected " );
	 }
	
}
	

	
	
}


