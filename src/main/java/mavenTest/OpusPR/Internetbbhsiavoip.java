package mavenTest.OpusPR;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import lib.ExcelDataConfig;

public class Internetbbhsiavoip {
	
	ExcelDataConfig excel = new ExcelDataConfig();
	
	public WebDriver internetPlanvoip(WebDriver driver)
	{
		try {
			Thread.sleep(4000);
			
			//WebElement clickInternet = driver.findElement(By.xpath("//*[@id='internetOffer']"));
			//clickInternet.click();
			System.out.println("selecting hsia card");
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
			
			js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@id='processCheckBox']")));
			
			
			System.out.println(" 12 month term Selected ");
			
			WebElement clickHSIAoffer = driver.findElement(By.xpath("//*[@id='productSelectedOfferForHsi']"));
			clickHSIAoffer.click();
			
		} 
		catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		
		return driver;
		
	}

}
