package mavenTest.OpusPR;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import lib.ExcelDataConfig;
import lib.Locators;
import lib.Screenshot;

public class BuildMyBundle {
	
	ExcelDataConfig excel = new ExcelDataConfig();
	Internet_BB internet = new Internet_BB();
	
	
	public WebDriver selectOffers(WebDriver driver)
		{
	try 
	{
		
		Thread.sleep(5000);	
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@id='processCheckBox']")));
		
		
		System.out.println(" 12 month term Selected "); 
		Screenshot.captureScreenshot(driver, "Avalible HSIA Cards");
		
		
		Thread.sleep(5000);
		
		WebElement clickHSIAoffer = driver.findElement(By.xpath("//*[@id='productSelectedOfferForHsi']"));
		clickHSIAoffer.click();
		
		System.out.println("HSIA selected");
		Thread.sleep(2000);
		

		
		Screenshot.captureScreenshot(driver, "validate order5");
		
		WebElement clickvalidateorder = driver.findElement(By.xpath("//*[@id='validateQuote']"));
		
		clickvalidateorder.click();
		
		
		
		Thread.sleep(4000);
					
		WebDriverWait oWait = new WebDriverWait (driver, 30);
		WebElement e = oWait.until(ExpectedConditions.visibilityOf(driver.findElement(Locators.monthlycharges)));
		
		System.out.println("Build my Bundle page Loaded. ");  
		
		Thread.sleep(2000);
		WebElement OPUS_csi_convid;
		OPUS_csi_convid=driver.findElement(Locators.csi_fed_convid);
		System.out.println("OPUS CSI CONV ID is :  "+ OPUS_csi_convid.getText());  
		
		
		
	WebElement clickvoipproduct= driver.findElement(By.xpath("//*[@id='phoneOffer']"));
		
		clickvoipproduct.click();
		Thread.sleep(4000);
	
	}
	
	catch (Exception e) 
	{
		
		e.printStackTrace();
	}
		return driver;
		
		}
}
	
	
	
	