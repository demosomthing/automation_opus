package lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataConfig {

	XSSFWorkbook wb;
	XSSFSheet sheet1;
	

	
	public String getData(int sheetnumber, int row, int column)
	{
		try {
			File dataFile =    new File(System.getProperty("user.dir")+".\\TestData\\"+"Test_Details.xlsx");
			FileInputStream fis = new FileInputStream(dataFile); 
			wb = new XSSFWorkbook(fis);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sheet1 = wb.getSheetAt(sheetnumber);
		String data="";
		
		
			 data = sheet1.getRow(row).getCell(column).getStringCellValue();
		
		
				return data;
	}
	
	public int getRowCount(int sheetindex)
	{
		int row = wb.getSheetAt(sheetindex).getLastRowNum();
		return row;
		
	}

}
